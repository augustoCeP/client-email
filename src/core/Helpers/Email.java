/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.Helpers;

import java.text.ParseException;

/**
 *
 * @author davi
 */
public class Email {
    private int id;
    private String from;
    private String subject;
    private String text;
    private String date;
    private String name;
    
    public String[] base64 = new String[10];
    public String[] fileName = new String[10];
    

    public Email(int id, String from, String subject, String text, String date, String name) {
        this.id = id;
        this.from = from;
        this.subject = subject;
        this.text = text;
        this.date = date;
        this.name = name;
    }
    
    public Email(String responseFromTop) throws ParseException {
        
        int fromIndex = responseFromTop.indexOf("From: ");
        
        this.name = responseFromTop.substring(fromIndex+6, responseFromTop.indexOf("<", fromIndex) );
        System.out.println("NOME DE QUEM MANDOU ---------->" + this.name);
        String fromEmail = responseFromTop.substring(fromIndex, responseFromTop.indexOf(">", fromIndex));
        this.from = fromEmail.substring(fromEmail.indexOf("<")+1);
        
        int subjectIndex = responseFromTop.indexOf("Subject: ");
        String subjectEmail = responseFromTop.substring(subjectIndex, responseFromTop.indexOf("\n", subjectIndex));
        this.subject = subjectEmail.substring(9);
        
        int dateIndex = responseFromTop.indexOf("Date: ");
        String dateEmail = responseFromTop.substring(dateIndex, responseFromTop.indexOf("-", dateIndex)-1);
        this.date = dateEmail.substring(6);
        
        
        
        
        
        
    }
    
    
    public void setBodyFromResponse(String response) {
        int startIndex = response.indexOf("charset=\"UTF-8\"");
        String body = "";
        String[] lines = null;
        if (startIndex > 0) {
            body = response.substring(startIndex, response.indexOf("--", startIndex));
            //System.err.println(body);
        }
        
        this.text = body.substring(16);
        if(this.text.contains("Content-Transfer-Encoding: quoted-printable")) {
            this.text = this.text.substring(this.text.indexOf("Content-Transfer-Encoding: quoted-printable")+45);
        }
        checkForAttachment(response);
    }
    
    private void checkForAttachment(String response) {
        System.err.println("-----------------------------------------------");
        System.err.println("-----------------------------------------------");
        System.err.println("-----------------------------------------------");
        System.err.println("-----------------------------------------------");
        System.err.println("-----------------------------------------------");
        int i =0;
        while(response.contains(("X-Attachment-Id"))) {
            int fileNameIndex = response.indexOf("Content-Disposition: attachment; filename=\"");
            fileName[i] = response.substring(fileNameIndex+43, response.indexOf("\"", fileNameIndex+43));
            
            int startIndex = response.indexOf("X-Attachment-Id");
            base64[i] = response.substring(response.indexOf("\n", startIndex),response.indexOf("--", startIndex) );
            response = response.substring(startIndex+15);
            i++;
           
        }
        System.err.print("NOME DE ATTACHMENT -------->  " + fileName[0]);
        
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    public int getID() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        return "from: " + this.from + "\n subject: "+this.subject+"\ndate: " +this.date;
    }
    
    
}
