package core.Helpers;

public class Logger {

    public static final String CONNECTING_ERROR = "Erro ao tentar se conectar";
    public static final String CONNECTION_SUCCESS = "Conectado com sucesso";
    public static final String COMMUNICATION_ERROR = "Erro ao se comunicar com o servidor";


    /**
     *
     * @param message corpo da mensagem
     */
    public static void write(String message){
        System.out.println("Logger/ " + message);
    }

    /**
     *
     * @param tag Tag para diferenciar melhor a mensagem
     * @param message corpo da mensagem
     */
    public static void write(String tag, String message){
        System.out.println("Logger/ "+ tag +"\n\t Message/ "+ message);
    }
}
