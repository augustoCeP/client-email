
package core.Helpers;

import java.util.ArrayList;

public class Utils {
    
    static public ArrayList<Integer> getEmailsIds(String str) {
        String[] lines = str.split(System.getProperty("line.separator"));
        ArrayList<Integer> ids = new ArrayList<>();
        for (int i = 0; i < lines.length; i++) {
            if(i==0) continue; //cabeçalho e espaço vazio
            ids.add(Integer.parseInt(lines[i].split(" ")[0]));
        }
        return ids;
    }
    
}
