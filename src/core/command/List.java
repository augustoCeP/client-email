/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.command;

import core.service.Connection;
import core.util.Request;
import core.util.ResponseCallback;

/**
 *
 * @author davi
 */
public class List implements Request{
    
    public static final String COMMAND = "LIST";

    @Override
    public void handle(String args, ResponseCallback responseCallback) {
        Connection.getInstance().sendCommand(args, responseCallback);
    }
    
}
