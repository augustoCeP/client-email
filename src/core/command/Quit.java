package core.command;

import core.Helpers.Logger;
import core.service.Connection;
import core.util.Request;
import core.util.ResponseCallback;


public class Quit implements Request {
    public static final String COMMAND = "QUIT";

    @Override
    public void handle(String args, ResponseCallback responseCallback) {
        Logger.write(getClass().getCanonicalName(), "Handle");
        Connection.getInstance().sendCommand(args, responseCallback);
    }

    
}
