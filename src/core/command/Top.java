
package core.command;

import core.service.Connection;
import core.util.Request;
import core.util.ResponseCallback;

public class Top implements Request{
    public static final String COMMAND = "TOP";

    @Override
    public void handle(String args, ResponseCallback responseCallback) {
        Connection.getInstance().sendCommand(args, responseCallback);
    }
    
}
