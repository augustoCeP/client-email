package core.factory;

import core.command.List;
import core.command.Pass;
import core.command.Quit;
import core.command.Retr;
import core.command.Top;
import core.command.User;
import core.util.Request;

/**
 *  Factory que vai instanciar as classes que herdam de request e implementam
 *  o metodo "handle" que vai de alguma forma comunicar com o servidor ou retornar
 *  uma string para uma outra classe fazer a conexão
 */

public class AbstractRequestFactory {

    public static Request getRequestInstance(String command){
        System.out.println("COMANDO -> "+command);
        switch (command){

            case Quit.COMMAND:
                return new Quit();
            case User.COMMAND:
                return new User();
            case Pass.COMMAND:
                return new Pass();
            case Retr.COMMAND:
                return  new Retr();
            case List.COMMAND:
                return new List();
            case Top.COMMAND:
                return new Top();

                
        }
        System.err.println("COMANDO NÃO ENCONTRADO");
        return null;
    }
}
