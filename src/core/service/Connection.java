package core.service;

import core.Helpers.Logger;
import core.util.ResponseCallback;

import java.io.*;

import javax.imageio.IIOException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.net.UnknownHostException;

public class Connection {

    private SSLSocket socket;
    public static String URL;
    public static int PORT;
    public static final String ERROR = "-ERR";
    public static final String OK = "+OK";
    private static Connection instance;
    private ResponseCallback responseCallback;
    private BufferedReader br;
    private OutputStream os;
    
    
    public Connection(String url, int port ) {
        this.URL = url;
        this.PORT = port;
    }

    public static Connection getInstance() {
        if(instance == null){
            instance = new Connection();
        }
        return instance;
    }

    private Connection() {
        try {
               System.out.println(Connection.URL + " " + Connection.PORT);
            socket = (SSLSocket) SSLSocketFactory.getDefault().createSocket(Connection.URL, Connection.PORT);

            os = socket.getOutputStream();
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            System.out.println(br.readLine());
        } catch (UnknownHostException e) {

            Logger.write(this.getClass().getCanonicalName(), Logger.CONNECTING_ERROR);
            e.printStackTrace();

        } catch (IOException e) {

            Logger.write(this.getClass().getCanonicalName(), Logger.CONNECTING_ERROR);
            e.printStackTrace();

        }

    }


    public void sendCommand(String command, ResponseCallback responseCallback) {
   Logger.write(command);
        try {
       os.write((command + "\n").getBytes());
       os.flush();
   }catch (IOException e){

   }
        this.responseCallback = responseCallback;
        getResponse();
    }


    public void getResponse() {

        String message = "" ;
        String thisLine = "";
        try {
            message = "";

            while ((thisLine = br.readLine()) != null) {

                if(thisLine.equals("+OK send PASS") || thisLine.equals("+OK Welcome.") || thisLine.equals(".")) break;
                message += thisLine + "\n";

            }
            
            Logger.write(message);

            responseCallback.onResponse(message);

        } catch (IOException e) {

            Logger.write(this.getClass().getCanonicalName(), Logger.COMMUNICATION_ERROR);
            e.printStackTrace();

        }

    }


}
