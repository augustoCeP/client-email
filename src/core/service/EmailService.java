package core.service;

import core.Helpers.Logger;
import core.factory.AbstractRequestFactory;
import core.util.Request;
import core.util.ResponseCallback;


/**
 * Classe para fazer a comunicação com o front e usará diretamente a classe connection.
 * Provavelmente so vai usar no trab 2.
 */

public class EmailService {

    public void sendRequest(String command, ResponseCallback responseCallback) {

        Request request = AbstractRequestFactory.getRequestInstance(command.substring(0, command.indexOf(" ")).toUpperCase());
        request.handle(command, responseCallback);


    }

}
