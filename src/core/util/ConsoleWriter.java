package core.util;

import core.Helpers.Logger;

import javax.swing.text.JTextComponent;
import java.io.*;

public class ConsoleWriter {
    private BufferedWriter bw;
    private FileWriter fw;
    private File f;
    private static final ConsoleWriter INSTANCE = new ConsoleWriter();
    private Observer observer;

    public static ConsoleWriter getInstance(){
        //if (instance == null){
          //  Logger.write("ConsoleWriter","null");
            //return new ConsoleWriter();
        //}else{
        //    Logger.write("ConsoleWriter",""+instance);
          //  return instance;
        //}
        return INSTANCE;
    }

    private ConsoleWriter() {
        try {
            f = new File("Console_Output.txt");
            if (!f.exists()) {
                f.createNewFile();
            }
            fw = new FileWriter(f);
            bw = new BufferedWriter(fw);
        } catch (IOException e) {
            e.printStackTrace();
            Logger.write(this.getClass().getCanonicalName(), "Erro ao instanciar writers");
        }

    }
    public void write(String text){
        observer.update(text);
    }


    public void writeToFile(JTextComponent component) {
        new Thread(  new Runnable() {
            @Override
            public void run() {
                try {
                    component.write(bw);
                    flush();
                } catch (IOException e) {
                    e.printStackTrace();
                    Logger.write(this.getClass().getCanonicalName(), "Erro ao escrever no arquivo");
                }
            }
        }).start();

    }

    public void flush() {
        try {
            bw.flush();
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
            Logger.write(this.getClass().getCanonicalName(), "Erro ao limpar writers");
        }
    }
    public void setObserver(Observer observer){
        this.observer = observer;
    }

}
