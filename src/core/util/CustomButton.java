
package core.util;

import javax.swing.JButton;


public class CustomButton extends JButton {
    
    private String tag;

    public CustomButton(String tag, String text) {
        super(text);
        this.tag = tag;
    }
    

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
    
}
