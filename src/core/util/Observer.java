package core.util;

public interface Observer {

  void update(String text);
}
