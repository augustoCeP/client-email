
package ui;

import core.Helpers.Email;
import core.Helpers.Logger;
import core.Helpers.Utils;
import core.command.List;
import core.command.Pass;
import core.command.Retr;
import core.command.Top;
import core.command.User;
import core.service.Connection;
import core.service.EmailService;
import core.util.CustomButton;
import core.util.ResponseCallback;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;

public class Main extends javax.swing.JFrame {
    EmailService emailService;
    ArrayList<Email> emails = new ArrayList<>();

    public Main() {
        emailService = new EmailService();
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblEmail = new javax.swing.JLabel();
        inputEmail = new javax.swing.JTextField();
        jButton1 = new CustomButton(User.COMMAND, "Enviar");
        lblPassword = new javax.swing.JLabel();
        lblEmailResponse = new javax.swing.JLabel();
        lblEmailReq = new javax.swing.JLabel();
        inputPassword = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblEmail.setText("Email: ");

        jButton1.setText("Enviar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        lblPassword.setText("Senha: ");
        lblPassword.setVisible(false);

        lblEmailResponse.setText("mail");
        lblEmailResponse.setVisible(false);

        lblEmailReq.setText("Email: ");
        lblEmailReq.setVisible(false);

        inputPassword.setVisible(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(lblEmail)
                                                        .addComponent(lblEmailReq))
                                                .addGap(18, 18, 18))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addComponent(lblPassword)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblEmailResponse, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(inputEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(inputPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(69, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblEmailResponse)
                                        .addComponent(lblEmailReq))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblEmail)
                                        .addComponent(inputEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblPassword)
                                        .addComponent(inputPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)
                                .addContainerGap(121, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        CustomButton cb = (CustomButton) evt.getSource();
        if (inputPassword.getPassword().length == 0) { // inseriu email
            String email = inputEmail.getText();
            emailService.sendRequest(cb.getTag() + " " + email, new ResponseCallback() {
                @Override
                public void onResponse(String... response) {
                    lblEmail.setVisible(false);
                    inputEmail.setVisible(false);
                    lblEmailResponse.setText(email);
                    lblEmailResponse.setVisible(true);
                    lblEmailReq.setVisible(true);
                    cb.setTag(Pass.COMMAND);

                    lblPassword.setVisible(true);
                    inputPassword.setVisible(true);
                }
            });
        } else { // inseriu senha
            System.out.println(inputPassword.getPassword());
            String password = new String(inputPassword.getPassword());
            emailService.sendRequest(cb.getTag() + " " + password, new ResponseCallback() {
                @Override
                public void onResponse(String... response) {

                    emailService.sendRequest(List.COMMAND+" ", new ResponseCallback() {
                        @Override
                        public void onResponse(String... response) {
                            ArrayList<Integer> ids = Utils.getEmailsIds(response[0]);
                            for(Integer id: ids) {
                               // System.out.println("ID -> " + id);
                                emailService.sendRequest(Top.COMMAND+" "+id+" "+0, new ResponseCallback() {
                                    @Override
                                    public void onResponse(String... response) {
                                        try {
                                            Email m = new Email(response[0]);
                                            m.setId(id);
                                            emails.add(m);
                                            
                                        } catch (ParseException ex) {
                                            java.util.logging.Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                  }
                                });
                           }
                            
                            Index index = new Index(emails, emailService);
                            index.setVisible(true);
                            dispose();
                        }
                    });
                   }
               
                
                
            });
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField inputEmail;
    private javax.swing.JPasswordField inputPassword;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblEmailReq;
    private javax.swing.JLabel lblEmailResponse;
    private javax.swing.JLabel lblPassword;
    // End of variables declaration//GEN-END:variables
}
