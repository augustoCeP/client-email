package ui.component;

import javax.swing.*;
import java.awt.*;

public class ConsoleOutput extends JTextArea {
    public ConsoleOutput(){
        this.setBackground(Color.BLACK);
        this.setForeground(Color.GREEN);
        this.setMargin(new Insets(2, 5, 2, 5));
        this.setFocusable(false);

    }

}
